#!/usr/bin/env cwltool

cwlVersion: v1.0
class: CommandLineTool
baseCommand: run_simulation.py

hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/sc4/run_simulation@sha256:bff357797c9497a18e9a1e3f21c6a8336b10e094bafbcc45b0c7f71a886cc5aa
    # this image is for linux/amd64

inputs:
  folder_root:
    type: string
    inputBinding:
      position: 1
  digit:
    type: int
    inputBinding:
      position: 2
  sample:
    type: int
    inputBinding:
      position: 3

outputs:
  sim_results:
    type: Directory
    outputBinding:
      glob: $(inputs.folder_root)


s:identifier: https://kg.ebrains.eu/api/instances/a574450b-1c30-458d-b7c4-5d55441ecb7d
s:keywords: ["simulation"]
s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0002-8306-0759
    s:name: Arnau Manasanch
  - class: s:Person
    s:identifier: https://kg.ebrains.eu/api/instances/714f39b8-9fd0-46cd-be4f-9112c87cfe3f
    s:name: Eleni Mathioulaki
s:codeRepository: https://gitlab.ebrains.eu/workflows/sc4_cwl
s:version: "v1.1"
s:dateCreated: "2023-07-14"
s:programmingLanguage: Python


$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
