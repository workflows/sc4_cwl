  #!/usr/bin/env cwltool

cwlVersion: v1.0
class: CommandLineTool
baseCommand: plot_results.py

hints:
  DockerRequirement:
    dockerPull: docker-registry.ebrains.eu/sc4/plot_results@sha256:d0214debe382e7cac6ef4f7861fc85950a2947b4cf07be623f32a3eb51156917
    # this image is for linux/amd64

inputs:
  folder_root:
    type: Directory
    inputBinding:
      position: 1
  plot_dir:
    type: string
    inputBinding:
      position: 2
  digit:
    type: int
    inputBinding:
      position: 3
  sample:
    type: int
    inputBinding:
      position: 4
  curr_t:
    type: int
    inputBinding:
      position: 5

outputs:
  plots:
    type: Directory
    outputBinding:
      glob: $(inputs.plot_dir)


s:identifier: https://kg.ebrains.eu/api/instances/72c9a63b-afe5-42bb-b95c-f0cc415906d4
s:keywords: ["data visualization"]
s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0002-8306-0759
    s:name: Arnau Manasanch
s:codeRepository: https://gitlab.ebrains.eu/workflows/sc4_cwl
s:version: "v1.1"
s:dateCreated: "2023-07-14"
s:programmingLanguage: Python


$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf