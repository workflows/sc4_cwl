#!/usr/bin/env python3

# IMPORTANT! for any changes here the docker image (docker-registry.ebrains.eu/tc/cwl-tools/bucket_fetch_file) has to be updated

import argparse
import shutil
import os
import requests as re
import matplotlib.pyplot as plt
import pickle
import inference
from datetime import datetime



def sc4_plot_inference(folder_root, plot_dir, digit, sample, curr_t=-1):
    
    if os.path.exists(plot_dir):
        shutil.rmtree(plot_dir)
    os.makedirs(plot_dir)

    # load inference images
    with open(folder_root + '/inf_imgs.pickle', 'rb') as f:
        live_imgs = pickle.load(f)

    digit = digit[0]
    sample = sample[0]

    curr_t = curr_t[0] # plot the final time of the simulation (350ms)

    image_shape = [(28, 28), (36, 36), (34, 34)]
    cols = ['{}'.format(col) for col in ['Input', 'Error', 'Prediction']]
    rows = ['Area {}'.format(str(row)) for row in range(3)]
    
    fig, axs = plt.subplots(nrows=3, ncols=3, figsize=(10,10))
    for i in range(3):
        print(live_imgs['pc' + str(i + 1)].shape)
        bu_img = inference.tf.reshape(live_imgs['pc' + str(i + 1)][curr_t, 0], image_shape[i])
        err_img = inference.tf.reshape(live_imgs['pc' + str(i + 1)][curr_t, 1], image_shape[i])
        td_img = inference.tf.reshape(live_imgs['pc' + str(i + 1)][curr_t, 2], image_shape[i])

        axs[i, 0].imshow(bu_img, vmin=600, vmax=3000, cmap="Reds")
        axs[i, 1].imshow(err_img, vmin=-3000, vmax=3000, cmap="bwr")
        axs[i, 2].imshow(td_img, vmin=600, vmax=3000, cmap="Reds")

    for jj in axs.flatten():
        jj.get_xaxis().set_ticks([])
        jj.get_yaxis().set_ticks([])
        
    for ax, col in zip(axs[0], cols):
        ax.set_title(col)

    for ax, row in zip(axs[:,0], rows):
        ax.set_ylabel(row, rotation='vertical', size='large')
        
    plt.show()


    # timestamp = int(datetime.now().timestamp()) # timestamp to avoid overwriting files in bucket
    plt.savefig(f'{os.getcwd()}/{plot_dir}/fig_traces_digit_{digit}_sample_{sample}.png')





# main    
parser = argparse.ArgumentParser()
parser.add_argument('folder_root', help='input folder')
parser.add_argument('plot_dir', help='plot dir name')
parser.add_argument('digit', type=int, nargs="+", help='b digit value')
parser.add_argument('sample', type=int, nargs="+", help='b sample value')
parser.add_argument('curr_t', type=int, nargs="+", help='b sim time')
args = parser.parse_args()
# run
sc4_plot_inference(args.folder_root, args.plot_dir, args.digit, args.sample, args.curr_t)
