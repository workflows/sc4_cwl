#!/usr/bin/env python3

import argparse
import shutil
import os, sys
import requests as re
import inference
import pickle
import numpy as np


def sc4_run_inference(folder_root, digit, sample):

    if os.path.exists(folder_root):
        shutil.rmtree(folder_root)
    os.makedirs(folder_root)

    digit = digit[0]
    sample = sample[0]

    # define folder root
    if not folder_root.startswith('/'):
        folder_root = '/' + folder_root 

    # load pre-trained weights
    w_mat = inference.load_and_convert_weights(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'weight_dict.pickle'))

    # build network
    snn_pc = inference.snn_pc(w_mat=w_mat)

    # load MNIST data
    (X_train, y_train), (X_test, y_test) = inference.tf.keras.datasets.mnist.load_data()
    X_train = X_train.astype('float32') / 255
    X_test = X_test.astype('float32') / 255

    # select digit
    if 0 <= digit <= 9:
        sample_indices = np.where(y_test == int(digit))[0]
    else:
        raise ValueError('Digit must be in the range [0-9]')

    # select sample
    if sample <= 100:
        sample_chosen = sample_indices[sample]
    else:
        raise ValueError('Sample must be in the range [0-100]')

    # get image
    input_mnist = X_test[sample_chosen].astype(np.float32)  
    
    # run inference
    snn_pc(input_mnist)

    # save results
    with open(os.getcwd() + folder_root + '/inf_imgs.pickle', 'wb') as f:
        pickle.dump(snn_pc.live_imgs, f)




parser = argparse.ArgumentParser()
parser.add_argument('folder_root', help='input folder')
parser.add_argument('digit', type=int, nargs="+", help='b digit value')
parser.add_argument('sample', type=int, nargs="+", help='b sample value')
args = parser.parse_args()

# run
sc4_run_inference(args.folder_root, args.digit, args.sample)
