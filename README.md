# **CWL integration of Showcase 4**

## Description
This is a CWL [Workflow](workflow.cwl) consisting of [2 steps (CommandLineTools)](steps).

This workflow demonstrates the performance of 3-layer spiking neural network for predictive coding. 

A user can select an MNIST sample, which the network has never seen before but can still make infererence based on other MNIST samples on which it has been trained.

Pre-defined inputs can be found in the [workflow_info.yaml](workflow_info.yml) file.

## Inputs 
folder_root: 'result'
plot_dir: 'output'
digit: 3
sample: 3
curr_t: 30

-  **folder_root**: results folder
-  **plot_dir**: directory where outputs will be stored
-  **digit**: digit of the MNIST dataset
-  **sample**: sample of the digit of the MNIST dataset
-  **curr_t**: inference progress at time curr_t

## Outputs
- **fig_traces_digit_{digit}_sample_{sample}.png**: final plot containing the inference progress of digit, sample and curr_t

## Steps

### Step 1: [Run Simulation](steps/run_simulation.cwl)

Runs the inference model

### Step 2: [Plotting Results](steps/plot_results.cwl) 

Plots the inference results




