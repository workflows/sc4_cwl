#!/usr/bin/env cwltool

cwlVersion: v1.0
class: Workflow

inputs:
  folder_root: string
  plot_dir: string
  digit: int
  sample: int
  curr_t: int

outputs:
  plots:
    type: Directory
    outputSource: visualization/plots

steps:
  simulation:
    run: steps/run_simulation.cwl
    in:
      folder_root: folder_root
      digit: digit
      sample: sample
    out: [sim_results]

  visualization:
    run: steps/plot_results.cwl
    in:
      folder_root: simulation/sim_results
      plot_dir: plot_dir
      digit: digit
      sample: sample
      curr_t: curr_t
    out: [plots]



s:identifier: https://kg.ebrains.eu/api/instances/86efc59a-b4d1-443d-a470-dd27ddc4465b
s:keywords: ["SC4", "Predictive Coding", "Inference"]
s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0002-8306-0759
    s:name: Arnau Manasanch
s:codeRepository: https://gitlab.ebrains.eu/workflows/sc4_cwl
s:version: "v1.1"
s:dateCreated: "2023-07-14"


$namespaces:
 s: https://schema.org/

$schemas:
 - https://schema.org/version/latest/schemaorg-current-http.rdf
